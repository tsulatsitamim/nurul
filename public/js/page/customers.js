/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/page/customers.js":
/***/ (function(module, exports) {

var dataTable = function () {
    var t = function t() {
        var t = $("#customers-table").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        method: "GET",
                        url: "http://tulip.manis/json/customers.json"
                    }
                }
            },
            layout: {
                theme: "default",
                class: "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            due: function due() {
                var data = t.dataSet;
                var arr = [];
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        var element = data[key];
                        if (element.hasOwnProperty("due")) {
                            arr.push(element.due);
                        }
                    }
                }
                // return Accounting.formatColumn([arr], "Rp ");
            },
            columns: [{
                field: "name",
                title: "Name"
            }, {
                field: "address",
                title: "Address"
            }, {
                field: "phone",
                title: "Phone",
                width: 200
            }, {
                field: "due",
                title: "Amount Due",
                width: 180,
                template: function template(data) {
                    // return `<div class="acconting">` + t.options.due()[0][data.getIndex()] + `</div>`;
                }
            }, {
                field: "Actions",
                width: 110,
                title: "Actions",
                sortable: !1,
                overflow: "visible",
                textAlign: 'center',
                template: function template(t) {
                    return "\n                        <a href=\"#\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill c-modal\" title=\"Edit\" data-course=\"" + t.id + "\" data-toggle=\"modal\" data-target=\"#modal-form\"><i class=\"la la-edit\"></i></a>\n\n                        <a href=\"#\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill pi-modal\" title=\"Statement\" data-course=\"" + t.id + "\" data-toggle=\"modal\" data-target=\"#modal-pi\"><i class=\"la la-bar-\tla-reorder\"></i></a>\n\n                        <div class=\"dropdown \">\n                            <button class=\"btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill c-has-dropdown\" data-toggle=\"dropdown\">\n                                <i class=\"la la-trash\"></i>\n                            </button>\n                            <div class=\"dropdown-menu dropdown-menu-right\">\n                                <a class=\"dropdown-item delete-resource\" href=\"/courses/delete/" + t.id + "\"><i class=\"la la-check\"></i> Delete</a>\n                                <a class=\"dropdown-item cancel\" href=\"#\"><i class=\"la la-remove\"></i> Cancel</a>\n                            </div>\n                        </div>\n                    ";
                }
            }]
        });
    };
    return {
        init: function init() {
            t();
        }
    };
}();

jQuery(document).ready(function () {
    dataTable.init();
});

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/page/customers.js");


/***/ })

/******/ });