FROM php:7.2-fpm


# Update packages and install composer and PHP dependencies.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libxml2-dev \
        libpng-dev \
        mysql-client \
        curl \
        git \
        nano \
    && pecl install apcu \
    && rm -rf /var/lib/apt/lists/*

# PHP Extensions
RUN docker-php-ext-install \
        pdo_mysql \
        tokenizer \
        xml \
        ctype \
        zip

# Memory Limit
RUN echo "memory_limit=2048M" > $PHP_INI_DIR/conf.d/memory-limit.ini
RUN echo "max_execution_time=900" >> $PHP_INI_DIR/conf.d/memory-limit.ini
RUN echo "extension=apcu.so" > $PHP_INI_DIR/conf.d/apcu.ini
RUN echo "post_max_size=50M" >> $PHP_INI_DIR/conf.d/memory-limit.ini
RUN echo "upload_max_filesize=50M" >> $PHP_INI_DIR/conf.d/memory-limit.ini

# Time Zone
RUN echo "date.timezone=Asia/Jakarta" > $PHP_INI_DIR/conf.d/date_timezone.ini

# Display errors in stderr
RUN echo "display_errors=stderr" > $PHP_INI_DIR/conf.d/display-errors.ini

# Disable PathInfo
RUN echo "cgi.fix_pathinfo=0" > $PHP_INI_DIR/conf.d/path-info.ini

# Disable expose PHP
RUN echo "expose_php=0" > $PHP_INI_DIR/conf.d/path-info.ini

RUN groupadd -g 1000 taher && \
    useradd -u 1000 -g taher -m taher && \
    usermod -p "*" taher

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


RUN rm /bin/sh && ln -s /bin/bash /bin/sh

USER taher

# nvm environment variables
RUN mkdir -p ~/.nvm
ENV NODE_VERSION stable
ENV NVM_DIR /home/taher/.nvm

# Install nvm
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash && \
    . $NVM_DIR/nvm.sh && \
    nvm install ${NODE_VERSION} && \
    nvm use ${NODE_VERSION} && \
    nvm alias ${NODE_VERSION} && \
    npm -v

RUN echo >> ~/.bashrc && \
    echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

# ARG YARN_VERSION=latest
# ENV YARN_VERSION ${YARN_VERSION}

# RUN [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" && \
#     curl -o- -L https://yarnpkg.com/install.sh | bash && \
#     echo >> ~/.bashrc && \
#     echo 'export PATH="$HOME/.yarn/bin:$PATH"' >> ~/.bashrc


USER root

RUN echo >> ~/.bashrc && \
    echo 'export NVM_DIR="/home/taher/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

# RUN echo >> ~/.bashrc && \
#     echo 'export YARN_DIR="/home/taher/.yarn"' >> ~/.bashrc && \
#     echo 'export PATH="$YARN_DIR/bin:$PATH"' >> ~/.bashrc

# Add node and npm to path so the commands are available by root
ENV PATH $PATH:$NVM_DIR/versions/node/v${NODE_VERSION}/bin

ADD php.ini /usr/local/etc/php/

USER taher