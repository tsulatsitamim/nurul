<?php

namespace App;

class Bill extends Model
{
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }

    public function items()
    {
        return $this->hasMany('App\BillItem');
    }

    public function payments()
    {
        return $this->hasMany('App\BillPay');
    }
}
