<?php

function setActive($path)
{
    return Request::is($path) ? 'm-menu__item--active' : '';
}