<?php

namespace App;

class Stock extends Model
{
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }

    public function bills()
    {
        return $this->hasMany('App\Bill');
    }

    public function stockadjs()
    {
        return $this->hasMany('App\StockAdj');
    }
}
