<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Invoice;
use Validator;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::with('invoices')->get();
        foreach ($customers as $customer) {
            $customer->accounting = [4];
            $customer->due = 0;
            foreach ($customer->invoices as $invoice) {

                $invoicePrice = $invoice->items->reduce(function ($carry, $value) {
                    return $carry + ($value->qty * $value->rate);
                }, 0);

                $invoiceDue = $invoicePrice - $invoice->payments->reduce(function ($carry, $value) {
                    return $carry + $value->payment;
                }, 0);

                $customer->due += $invoiceDue;
            }
        }
        return response()->json($customers);
    }

    public function debt()
    {
        $invoices = Invoice::with('customer', 'items.stock', 'payments')->get();
        $filtered = $invoices->map(function($value){
            $price = $value->items->reduce(function ($carry, $value) {
                return $carry + ($value->qty * $value->rate);
            }, 0);
            $payment = $value->payments->reduce(function ($carry, $value) {
                return $carry + $value->payment;
            }, 0);
            $value->price = $price;
            $value->payment = $payment;
            if ($payment < $price) {
                return $value;
            }
        });

        $filtered = $filtered->filter()->groupBy('customer_id')->map(function($value){
            if (isset($value[0])) {
                $data['id'] = $value[0]->customer_id;
                $data['name'] = $value[0]->customer->name;
                $data['price'] = $value->reduce(function ($carry, $val) {
                    return $carry + $val->price;
                }, 0);
                $data['payment'] = $value->reduce(function ($carry, $val) {
                    return $carry + $val->payment;
                }, 0);
                $data['debt'] = $data['price'] - $data['payment'];
                $data['invoices'] = $value;
                return $data;
            }

        })->values();

        return response()->json($filtered);
    }

    public function store(Request $request)
    {
        $request = $request->all();
        $request['nama'] = $request['name'];
        $messages = [
            'required' => 'Mohon masukan :attribute.',
        ];
        $validator = Validator::make($request, [
            'nama' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }
        $customer = Customer::create([
            'name' => $request['nama'],
            'phone' => $request['phone'],
            'address' => $request['address']
        ]);
        return response()->json($customer);
    }

    public function show($id)
    {
        $customer = Customer::find($id);
        return response()->json($customer);
    }

    public function update(Request $request)
    {
        $request = $request->all();
        $request['nama'] = $request['name'];
        $messages = [
            'required' => 'Mohon masukan :attribute.',
        ];
        $validator = Validator::make($request, [
            'nama' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }
        $customer = Customer::find($request['id']);
        $customer->update([
            'name' => $request['nama'],
            'phone' => $request['phone'],
            'address' => $request['address']
        ]);
        return response(null, 202);
    }

    public function destroy($id)
    {
        $invoices = Invoice::where('customer_id', '=', $id)
            ->get()
            ->groupBy('id')
            ->keys();
        if ($invoices->isNotEmpty()) {
            return response()->json('Customer tidak bisa dihapus karena digunakan dalam penjualan no ' . $invoices->implode(', '), 422);
        }

        $customer = Customer::find($id)->delete();
        return response(null, 202);
    }
}
