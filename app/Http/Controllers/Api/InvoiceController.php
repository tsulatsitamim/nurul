<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\InvoiceItem;
use App\InvoicePay;
use App\Stock;
use App\Cash;
use Carbon\Carbon;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        $query = Invoice::with('customer', 'items.stock', 'payments');
        if ($request->start) {
            $query = Invoice::whereBetween('created_at', [$request->start, $request->end])->with('customer', 'items.stock', 'payments');
        }
        $invoices = $query->get();
        $invoices->each(function($item){
            $vendor = $item['customer']['name'];
            unset($item->vendor);
            $item->vendor = $vendor;
            $item->price = $item->items->reduce(function ($carry, $value) {
                return $carry + ($value->qty * $value->rate);
            }, 0);
            $item->payment = $item->payments->reduce(function ($carry, $value) {
                return $carry + $value->payment;
            }, 0);
            $item->due = $item->price - $item->payment;

        });
        return response()->json($invoices);
    }

    public function store(Request $request)
    {
        if (!array_key_exists('customer', $request[0])) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['customer' => ['The customer field is required.']]
            ], 422);
        }

        if (is_null($request[0]['customer'])) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['customer' => ['The customer field is required.']]
            ], 422);
        }

        $invoice = Invoice::create([
            'customer_id' => $request[0]['customer'],
            'created_at' => $request[0]['date']
        ]);

        $items = [];
        if ($request[1]) {
            foreach ($request[1] as $key => $item) {
                if (!is_null($item['stock_id'])) {
                    $items[] = new InvoiceItem(['stock_id' => $item['stock_id'], 'scale' => $item['scale'], 'qty' => $item['qty'], 'rate' => $item['rate'], 'ket' => $item['ket']]);
                }
            }
        }

        $invoice->items()->saveMany($items);

        foreach ($invoice->items()->get() as $item) {
            $stock = Stock::find($item->stock_id);
            $stock->update(['qty' => $stock->qty - $item->qty]);
        }

        $payments = [];
        if ($request[2]) {
            foreach ($request[2] as $key => $item) {
                if (!is_null($item['payment'])) {
                    $payments[] = new InvoicePay(['payment' => $item['payment'], 'cash_id' => $item['cash_id'],'created_at' => $item['created_at']]);
                }
            }
        }
        $invoice->payments()->saveMany($payments);
        foreach ($invoice->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount + $item->payment]);
        }
        
        return response(null, 202);
    }

    public function show($id)
    {
        $invoice = Invoice::with('customer', 'items', 'payments')->find($id);
        return response()->json($invoice);
    }

    public function update(Request $request)
    {
        
        $invoice = Invoice::find($request[0]['id']);
        $invoice->update([
            'created_at' => $request[0]['date']
        ]);

        foreach ($invoice->items()->get() as $item) {
            $stock = Stock::find($item->stock_id);
            $stock->update(['qty' => $stock->qty + $item->qty]);
        }

        InvoiceItem::where('invoice_id', '=', $request[0]['id'])->delete();
        $items = [];
        if ($request[1]) {
            foreach ($request[1] as $key => $item) {
                if (!is_null($item['stock_id'])) {
                    $items[] = new InvoiceItem(['stock_id' => $item['stock_id'], 'scale' => $item['scale'], 'qty' => $item['qty'], 'rate' => $item['rate'], 'ket' => $item['ket']]);
                }
            }
        }
        $invoice->items()->saveMany($items);
        foreach ($invoice->items()->get() as $item) {
            $stock = Stock::find($item->stock_id);
            $stock->update(['qty' => $stock->qty - $item->qty]);
        }

        foreach ($invoice->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount - $item->payment]);
        }
        InvoicePay::where('invoice_id', '=', $request[0]['id'])->delete();
        $payments = [];
        if ($request[2]) {
            foreach ($request[2] as $key => $item) {
                if (!is_null($item['payment'])) {
                    $payments[] = new InvoicePay(['payment' => $item['payment'], 'cash_id' => $item['cash_id'], 'created_at' => $item['created_at']]);
                }
            }
        }
        $invoice->payments()->saveMany($payments);
        foreach ($invoice->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount + $item->payment]);
        }

        return response(null, 202);
    }

    public function destroy($id)
    {
        $invoice = Invoice::find($id);

        foreach ($invoice->items()->get() as $item) {
            $stock = Stock::find($item->stock_id);
            $stock->update(['qty' => $stock->qty + $item->qty]);
        }

        foreach ($invoice->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount - $item->payment]);
        }
        
        InvoiceItem::where('invoice_id', '=', $id)->delete();
        InvoicePay::where('invoice_id', '=', $id)->delete();
        $invoice->delete();
        return response(null, 202);
    }

    public function storePayment(Request $request)
    {

        $invoicePay = InvoicePay::create([
            'payment' => $request->payment,
            'invoice_id' => $request->invoice_id,
            'created_at' => Carbon::parse($request->created_at)->setTimezone('Asia/Jakarta')
        ]);
        return response(null, 202);
    }

    public function destroyPayment($id)
    {
        InvoicePay::find($id)->delete();
        return response(null, 202);
    }
}
