<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\InvoiceItem;
use App\InvoicePay;
use App\Bill;
use App\BillPay;
use App\OtherPay;
use App\Stock;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function dashboard(Request $request){
        if ($request->day) {
            if ($request->day == 1) {
                $invoices = Invoice::with('items')->whereDate('created_at', '=', Carbon::now('Asia/Jakarta')->toDateString())->get();
            }

            return response()->json([
                'invoices' => $invoices
            ]);
        }
        $invoices = Invoice::with('customer', 'items', 'payments')->get();
        $invoices->each(function($item){
            $vendor = $item['customer']['name'];
            unset($item->vendor);
            $item->vendor = $vendor;
            $item->price = $item->items->reduce(function ($carry, $value) {
                return $carry + ($value->qty * $value->rate);
            }, 0);
            $item->due = $item->price - $item->payments->reduce(function ($carry, $value) {
                return $carry + $value->payment;
            }, 0);

        });

        return 'end';
    }

    public function jurnal(Request $request){
        if ($request->start && $request->end) {
            $billpays = BillPay::whereBetween('created_at', [$request->start, $request->end])
                ->with('bill.vendor')
                ->get();
            $invoicepays = InvoicePay::whereBetween('created_at', [$request->start, $request->end])
                ->with('invoice.customer')
                ->get();
            $otherpays = OtherPay ::whereBetween('created_at', [$request->start, $request->end])
                ->get();

            $bps = $billpays->map(function($item){
                return collect([
                    'ket' => $item->bill->vendor->name,
                    'payment' => $item->payment,
                    'date' => $item->created_at->format('Y-m-d H:i:s'),
                    'flow' => 1
                ]);
            });
            
            $ips = $invoicepays->map(function($item){
                return collect([
                    'ket' => $item->invoice->customer->name,
                    'payment' => $item->payment,
                    'date' => $item->created_at->format('Y-m-d H:i:s'),
                    'flow' => 0
                ]);
            });
            
            $ops = $otherpays->map(function($item){
                return collect([
                    'ket' => $item->ket,
                    'payment' => $item->price,
                    'date' => $item->created_at->format('Y-m-d H:i:s'),
                    'flow' => 1
                ]);
            });

            $jurnal = $bps->concat($ips)->concat($ops)->sortBy('date')->values()->all();

            return $jurnal;
        }
        return BillPay::all();
    }

    public function invoice(Request $request){
        $invoices = Invoice::with('items', 'payments')->get();

        $invoices->each(function($item){
            $vendor = $item['customer']['name'];
            $item->price = $item->items->reduce(function ($carry, $value) {
                return $carry + ($value->qty * $value->rate);
            }, 0);
            $item->payment = $item->payments->reduce(function ($carry, $value) {
                return $carry + $value->payment;
            }, 0);
            $item->due = $item->price - $item->payment;
            unset($item->vendor);
            unset($item->items);
            unset($item->payments);
            unset($item->customer);
            unset($item->customer_id);
            unset($item->updated_at);

        });

        $invoiceGroup = [];
        foreach ($invoices as $invoice) {
            $invoiceGroup['price'][substr($invoice->created_at,0,4)][substr($invoice->created_at,5,2)][] = $invoice->price;
            $invoiceGroup['payment'][substr($invoice->created_at,0,4)][substr($invoice->created_at,5,2)][] = $invoice->payment;
        }

        foreach ($invoiceGroup['price'] as $year => $invoiceMonth) {
            foreach ($invoiceMonth as $month => $imonth) {
                $invoiceGroup['price'][$year][$month] = collect($imonth)->sum();
            }
        }

        foreach ($invoiceGroup['payment'] as $year => $invoiceMonth) {
            foreach ($invoiceMonth as $month => $imonth) {
                $invoiceGroup['payment'][$year][$month] = collect($imonth)->sum();
            }
        }

        return response()->json($invoiceGroup);
    }
}
