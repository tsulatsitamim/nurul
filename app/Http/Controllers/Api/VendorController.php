<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vendor;
use App\Bill;
use App\Stock;
use Validator;
use Illuminate\Support\Facades\DB;

class VendorController extends Controller
{
    public function index()
    {
        $vendors = Vendor::with('stocks')->get()->sortBy('name')->values()->all();
        return response()->json($vendors);
    }

    public function store(Request $request)
    {
        $request = $request->all();
        $request['nama'] = $request['name'];
        $messages = [
            'required' => 'Mohon masukan :attribute.',
        ];
        $validator = Validator::make($request, [
            'nama' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        if (!array_key_exists('phone', $request)) {
            $vendor = Vendor::create([
                'name' => $request['nama']
            ]);
            return response()->json($vendor);
        }

        $vendor = Vendor::create([
            'name' => $request['nama'],
            'phone' => $request['phone'],
            'address' => $request['address']
        ]);
        
        return response()->json($vendor);
    }

    public function show($id)
    {
        $vendor = Vendor::find($id);
        return response()->json($vendor);
    }

    public function update(Request $request)
    {
        $request = $request->all();
        $request['nama'] = $request['name'];
        $messages = [
            'required' => 'Mohon masukan :attribute.',
        ];
        $validator = Validator::make($request, [
            'nama' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $vendor = Vendor::find($request['id']);
        $vendor->update([
            'name' => $request['nama'],
            'phone' => $request['phone'],
            'address' => $request['address']
        ]);
        return response(null, 202);
    }

    public function destroy($id)
    {
        $stocks = Stock::where('vendor_id', '=', $id)->get();
        $stock_ids = $stocks->groupBy('id')->keys();
        $invoices = DB::table('invoice_items')
                    ->whereIn('stock_id', $stock_ids)
                    ->get()
                    ->groupBy('invoice_id')
                    ->keys();
        if ($invoices->isNotEmpty()) {
            return response()->json('Supplier tidak bisa dihapus karena digunakan dalam penjualan no ' . $invoices->implode(', '), 422);
        }

        $bills = Bill::where('vendor_id', '=', $id)
            ->get()
            ->groupBy('id')
            ->keys();
        if ($bills->isNotEmpty()) {
            return response()->json('Supplier tidak bisa dihapus karena digunakan dalam pembelian no ' . $bills->implode(', '), 422);
        }

        Stock::where('vendor_id', '=', $id)->delete();
        Vendor::find($id)->delete();
        return response(null, 202);
    }
}
