<?php

namespace App;

class BillPay extends Model
{
    public function bill()
    {
        return $this->belongsTo('App\Bill');
    }
}
