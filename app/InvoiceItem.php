<?php

namespace App;

class InvoiceItem extends Model
{
    public function stock()
    {
        return $this->belongsTo('App\Stock');
    }
}
