<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/pengaturan/kas', 'Api\CashController@index');
Route::post('/pengaturan/kas', 'Api\CashController@store');
Route::get('/pengaturan/kas/{id}', 'Api\CashController@show');
Route::put('/pengaturan/kas/{id}', 'Api\CashController@update');

Route::get('/report/dashboard', 'Api\DashboardController@dashboard');
Route::post('/laporan/jurnal', 'Api\DashboardController@jurnal');
Route::get('/laporan/jurnal', 'Api\DashboardController@jurnal');

Route::get('/dashboard/invoices', 'Api\DashboardController@invoice');

Route::get('/other', 'Api\OtherPayController@index');
Route::post('/other', 'Api\OtherPayController@store');
Route::get('/other/{id}', 'Api\OtherPayController@show');
Route::put('/other/{id}', 'Api\OtherPayController@update');
Route::delete('/other/{id}', 'Api\OtherPayController@destroy');

Route::get('/invoices', 'Api\InvoiceController@index');
Route::post('/invoices', 'Api\InvoiceController@store');
Route::get('/invoices/{id}', 'Api\InvoiceController@show');
Route::put('/invoices/{id}', 'Api\InvoiceController@update');
Route::delete('/invoices/{id}', 'Api\InvoiceController@destroy');

Route::get('/customers', 'Api\CustomerController@index');
Route::get('/customers/debt', 'Api\CustomerController@debt');
Route::post('/customers', 'Api\CustomerController@store');
Route::get('/customers/{id}', 'Api\CustomerController@show');
Route::put('/customers/{id}', 'Api\CustomerController@update');
Route::delete('/customers/{id}', 'Api\CustomerController@destroy');

Route::get('/vendors', 'Api\VendorController@index');
Route::post('/vendors', 'Api\VendorController@store');
Route::get('/vendors/{id}', 'Api\VendorController@show');
Route::put('/vendors/{id}', 'Api\VendorController@update');
Route::delete('/vendors/{id}', 'Api\VendorController@destroy');

Route::get('/bills', 'Api\BillController@index');
Route::post('/bills', 'Api\BillController@store');
Route::get('/bills/{id}', 'Api\BillController@show');
Route::put('/bills/{id}', 'Api\BillController@update');
Route::delete('/bills/{id}', 'Api\BillController@destroy');

Route::post('/bills/add/payment', 'Api\BillController@storePayment');
Route::delete('/bills/payment/{id}', 'Api\BillController@destroyPayment');

Route::post('/invoices/add/payment', 'Api\InvoiceController@storePayment');
Route::delete('/invoices/payment/{id}', 'Api\InvoiceController@destroyPayment');

Route::get('/stocks/index/{id}', 'Api\StockController@indexAdj');
Route::post('/stocks/add/adj', 'Api\StockController@storeAdj');

Route::get('/stocks', 'Api\StockController@index');
Route::post('/stocks', 'Api\StockController@store');
Route::get('/stocks/{id}', 'Api\StockController@show');
Route::put('/stocks/{id}', 'Api\StockController@update');
Route::delete('/stocks/{id}', 'Api\StockController@destroy');
