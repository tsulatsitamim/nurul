<?php

use Faker\Generator as Faker;

$factory->define(\App\Customer::class, function (Faker $faker) {
    return [
        'phone' => $faker->phoneNumber,
        'name' => $faker->name,
        'address' => $faker->address
    ];
});

$factory->define(\App\Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->Company,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address
    ];
});

$factory->define(\App\Stock::class, function (Faker $faker) {
    $input = ["GMP", "SUJ", "Gulaku", "MSI Medan", "KBA"];
    return [
        'name' => $input[array_rand($input, 1)],
        'qty' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2000)
    ];
});

$factory->define(\App\Bill::class, function (Faker $faker) {
    return [
        'due' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10000000, $max = 500000000),
        'qty' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2000),
    ];
});