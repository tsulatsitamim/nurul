<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScaleToBillItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_items', function (Blueprint $table) {
            $table->integer('scale')->default(1);
        });
        Schema::table('invoice_items', function (Blueprint $table) {
            $table->integer('scale')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_items', function (Blueprint $table) {
            $table->dropColumn('scale');
        });
        Schema::table('invoice_items', function (Blueprint $table) {
            $table->dropColumn('scale');
        });
    }
}
