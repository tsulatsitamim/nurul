
import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '../views/Dashboard.vue';
import Invoice from '../views/Invoice.vue';
import Customer from '../views/Customer.vue';
import Vendor from '../views/Vendor.vue';
import Bill from '../views/Bill.vue';
import Stock from '../views/Stock.vue';
import Hutang from '../views/Hutang.vue';
import Jurnal from '../views/Jurnal.vue';
import Penjualan from '../views/Penjualan.vue';
import Pembelian from '../views/Pembelian.vue';
import Laba from '../views/Laba.vue';
import Other from '../views/Other.vue';
import Other2 from '../views/Other2.vue';
import Cash from '../views/Cash.vue';

import About from '../views/About.vue';
import Home from '../views/Home.vue';

Vue.use(Router);

const routes = [{
        path: '/dashboard',
        name: 'dashboard2',
        component: Dashboard
    },{
        path: '/',
        name: 'dashboard',
        component: Dashboard
    },{
        path: '/penjualan',
        name: 'invoices',
        component: Penjualan
    },{
        path: '/pembelian',
        name: 'bills',
        component: Pembelian
    },{
        path: '/lainnya',
        name: 'other',
        component: Other
    },{
        path: '/lainnya2',
        name: 'other2',
        component: Other2
    },{
        path: '/pelanggan',
        name: 'customers',
        component: Customer
    },{
        path: '/supplier',
        name: 'vendors',
        component: Vendor
    },{
        path: '/produk',
        name: 'stocks',
        component: Stock
    },{
        path: '/laporan/hutang',
        name: 'hutang',
        component: Hutang
    },{
        path: '/laporan/jurnal',
        name: 'jurnal',
        component: Jurnal
    },{
        path: '/laporan/penjualan',
        name: 'penjualan',
        component: Penjualan
    },{
        path: '/laporan/pembelian',
        name: 'pembelian',
        component: Pembelian
    },{
        path: '/laporan/labarugi',
        name: 'laba',
        component: Laba
    },{
        path: '/pengaturan/kas',
        name: 'kas',
        component: Cash
    }
    
    ,{
        path: '/',
        name: 'home',
        component: Home
    },{
        path: '/about',
        name: 'about',
        component: About
    }
];

export default new Router({
    mode: 'history',
    linkActiveClass: 'm-menu__item--active',
    linkExactActiveClass: '',
    routes
});