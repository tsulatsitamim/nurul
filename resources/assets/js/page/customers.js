var dataTable = function () {
    var t = function () {
        var t = $("#customers-table").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        method: "GET",
                        url: "http://tulip.manis/json/customers.json"
                    }
                }
            },
            layout: {
                theme: "default",
                class: "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            due: () => {
                const data = t.dataSet;
                let arr = [];
                for (const key in data) {
                    if (data.hasOwnProperty(key)) {
                        const element = data[key];
                        if (element.hasOwnProperty("due")) {
                            arr.push(element.due);
                        }
                    }
                }
                // return Accounting.formatColumn([arr], "Rp ");
            },
            columns: [{
                field: "name",
                title: "Name"
            }, {
                field: "address",
                title: "Address"
            }, {
                field: "phone",
                title: "Phone",
                width: 200
            }, {
                field: "due",
                title: "Amount Due",
                width: 180,
                template: (data) => {
                    // return `<div class="acconting">` + t.options.due()[0][data.getIndex()] + `</div>`;
                }
            }, {
                field: "Actions",
                width: 110,
                title: "Actions",
                sortable: !1,
                overflow: "visible",
                textAlign: 'center',
                template: function (t) {
                    return `
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill c-modal" title="Edit" data-course="` + t.id + `" data-toggle="modal" data-target="#modal-form"><i class="la la-edit"></i></a>

                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill pi-modal" title="Statement" data-course="` + t.id + `" data-toggle="modal" data-target="#modal-pi"><i class="la la-bar-	la-reorder"></i></a>

                        <div class="dropdown ">
                            <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill c-has-dropdown" data-toggle="dropdown">
                                <i class="la la-trash"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item delete-resource" href="/courses/delete/` + t.id + `"><i class="la la-check"></i> Delete</a>
                                <a class="dropdown-item cancel" href="#"><i class="la la-remove"></i> Cancel</a>
                            </div>
                        </div>
                    `
                }
            }]
        });
    };
    return {
        init: function () {
            t()
        }
    }
}();

jQuery(document).ready(function () {
    dataTable.init();
});