<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ isset($data['title']) ? $data['title'] . " - CV Tulip Manis" : "CV Tulip Manis" }}</title>
        <meta name="description" content="DTMI Abet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <!-- favicon -->
        <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
        
    </head>
    <body>
        <div id="app"></div>
        <script src="{{ mix('/js/vendors.bundle.js') }}"></script>
        <script src="{{ mix('/js/scripts.bundle.js') }}"></script>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
