<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
  <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

  <!-- BEGIN: Aside Menu -->
  <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
    
      <li class="m-menu__item  m-menu__item {{ setActive('/')}} {{ setActive('report*')}}" aria-haspopup="true" >
        <a  href="/" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-line-graph"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Dashboard
              </span>
            </span>
          </span>
        </a>
      </li>

      <li class="m-menu__section">
        <h4 class="m-menu__section-text">
          Sales
        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
      </li>
      <li class="m-menu__item {{ setActive('invoices*')}}" aria-haspopup="true">
        <a  href="/invoices" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-cart"></i>
          <span class="m-menu__link-text">
            Invoices
          </span>
        </a>
      </li>
      <li class="m-menu__item {{ setActive('customers*')}}" aria-haspopup="true">
        <a  href="/customers" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-users"></i>
          <span class="m-menu__link-text">
            Customers
          </span>
        </a>
      </li>

      <li class="m-menu__section">
        <h4 class="m-menu__section-text">
          Purchases
        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
      </li>
      <li class="m-menu__item {{ setActive('bills*')}}" aria-haspopup="true">
        <a  href="/bills" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-coins"></i>
          <span class="m-menu__link-text">
            Bills
          </span>
        </a>
      </li>
      <li class="m-menu__item {{ setActive('vendors*')}}" aria-haspopup="true">
        <a  href="/vendors" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-truck"></i>
          <span class="m-menu__link-text">
            Vendors
          </span>
        </a>
      </li>

      <li class="m-menu__section">
        <h4 class="m-menu__section-text">
          Products
        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
      </li>
      <li class="m-menu__item {{ setActive('Stock*')}}" aria-haspopup="true">
        <a  href="/Stock" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-coins"></i>
          <span class="m-menu__link-text">
            Stock
          </span>
        </a>
      </li>

    </ul>
  </div>
  <!-- END: Aside Menu -->
</div>