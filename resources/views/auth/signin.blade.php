@extends('layout.html')

@section('body')

<body class="m--skin- m-page--loading m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
       
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(images/bg/bg.jpg);">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="/images/logo-2.png" style="width: 200px;">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Login to Dashboard
								</h3>
							</div>
							<form class="m-login__form m-form"  method="POST" action="/login" id="login-form">
								{{ csrf_field() }}
								<div class="form-group m-form__group has-danger">
									<input class="form-control m-input {{ $errors->has('email') ? 'form-control-danger' : '' }}" type="text" placeholder="Email" name="email" value="{{ old('email') }}">
									@if($errors->has('email'))
											<div class="form-control-feedback">
												{{ $errors->first('email') }}
											</div>
									@endif
								</div>
								<div class="form-group m-form__group has-danger">
									<input class="form-control m-input m-login__form-input--last {{ $errors->has('password') ? 'form-control-danger' : '' }}" type="password" placeholder="Password" name="password">
									@if($errors->has('password'))
											<div class="form-control-feedback">
												{{ $errors->first('password') }}
											</div>
									@endif
								</div>
								<div class="row m-login__form-sub" style="visibility:hidden" >
									<div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--focus">
											<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} checked>
											Remember Me
											<span></span>
										</label>
									</div>
									
								</div>
								<div class="m-login__form-action">
									<button type="submit" form="login-form" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
										Sign In
									</button>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!--begin::Base Scripts -->
		<script src="{{ mix('/js/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ mix('/js/scripts.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>

		<!--end::Base Scripts -->
		@yield('contentscript')
		
		<!-- end::Page Loader -->
	</body>
	<!-- end::Body -->
@endsection